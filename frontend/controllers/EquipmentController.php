<?php

namespace frontend\controllers;

use backend\models\Producer;
use Yii;
use backend\models\Equipment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * EquipmentController implements the CRUD actions for Equipment model.
 */
class EquipmentController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionView($url)
    {
        $this->layout = 'article';
        $model = $this->findUrl($url);
        $title = (Yii::$app->language == 'ru') ? $model->title_ru : ((Yii::$app->language == 'en') ? $model->title_en : $model->title_kz);
        $keywords = (Yii::$app->language == 'ru') ? $model->keywords_ru : ((Yii::$app->language == 'en') ? $model->keywords_en : $model->keywords_kz);
        $description = (Yii::$app->language == 'ru') ? $model->desc_ru : ((Yii::$app->language == 'en') ? $model->desc_en : $model->desc_kz);
        $this->setMeta($title, $keywords, $description);
        $equipments = Equipment::find()->where(['status' => 1])->all();
        $producers = Producer::find()->where(['eq_id' => $model->id])->all();
        return $this->render('view', [
            'model' => $model, 'equipments' => $equipments, 'producers' => $producers,
        ]);
    }

    protected function findUrl($url)
    {
        if (($model = Equipment::find()->where(['url' => $url, 'status' => 1])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
