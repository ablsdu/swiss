<?php
/**
 * Created by PhpStorm.
 * User: wew
 * Date: 27.08.2016
 * Time: 22:32
 */
namespace frontend\controllers;
use yii\web\Controller;

class AppController extends Controller{

    protected function setMeta($title = null, $keywords = null, $description = null){
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
        $this->view->registerMetaTag(['property' => 'og:author', 'content' => "SECURETECH.KZ"]);
        $this->view->registerMetaTag(['property' => 'og:title', 'content' => "$title"]);
        $this->view->registerMetaTag(['property' => 'og:description', 'content' => "$description"]);
    }
}