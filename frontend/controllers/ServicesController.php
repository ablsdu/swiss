<?php

namespace frontend\controllers;

use Yii;
use backend\models\Services;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * ServicesController implements the CRUD actions for Services model.
 */
class ServicesController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionView($url)
    {
        $this->layout = 'services';
        $model = $this->findUrl($url);
        $title = (Yii::$app->language == 'ru') ? $model->title_ru : ((Yii::$app->language == 'en') ? $model->title_en : $model->title_kz);
        $keywords = (Yii::$app->language == 'ru') ? $model->keywords_ru : ((Yii::$app->language == 'en') ? $model->keywords_en : $model->keywords_kz);
        $description = (Yii::$app->language == 'ru') ? $model->desc_ru : ((Yii::$app->language == 'en') ? $model->desc_en : $model->desc_kz);
        $this->setMeta($title, $keywords, $description);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    protected function findUrl($url)
    {
        if (($model = Services::find()->where(['url' => $url, 'status' => 1])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
