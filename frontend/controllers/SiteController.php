<?php
namespace frontend\controllers;

use backend\models\AboutGroup;
use backend\models\Certificates;
use backend\models\Equipment;
use backend\models\Pages;
use backend\models\Services;
use backend\models\ServicesFullSearch;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * Site controller
 */
class SiteController extends AppController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $equipments = Equipment::find()->where(['status' => 1])->orderBy(['queue' => SORT_ASC])->all();
        $services = Services::find()->where(['status' => 1])->all();
        $certificates = Certificates::find()->all();
        $about = Pages::findOne(1);
        $group = Pages::findOne(2);
        return $this->render('index',[
            'equipments' => $equipments,
            'services' => $services,
            'certificates' => $certificates,
            'about' => $about,
            'group' => $group,
        ]);
    }

    /**
     * Displays about group page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $tab_model = AboutGroup::find()->all();
        return $this->render('about-group',[
            'tab_model' => $tab_model,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionSearch()
    {
        $this->layout = 'article';
        $searchModel = new ServicesFullSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== "<action>");
        return parent::beforeAction($action);
    }
    public function actionCall()
    {
        $name = Yii::$app->request->get('name');
        $phone = Yii::$app->request->get('phone');
        $email = Yii::$app->request->get('email');
        $msg = Yii::$app->request->get('msg');;
        if (Yii::$app->request->get('email') || Yii::$app->request->get('phone')) {
            $body = "Имя: " . $name . "<br/>Телефон: " . $phone . "<br/>Email: " . $email . "<br/>Сообщение: " . $msg . "<br/>";
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['adminEmail'] => 'Swiss Commerce & Engineering'])
                ->setTo(Yii::$app->params['salesEmail'])
                ->setSubject('Запрос на получение комм. предложения с сайта Swiss Commerce & Engineering')
                ->setHtmlBody($body)
                ->send();
        }
    }

    public function actionMessage()
    {
        $name = Yii::$app->request->get('name1');
        $email = Yii::$app->request->get('email1');
        $msg = Yii::$app->request->get('msg1');;
        if (Yii::$app->request->get('email1')) {
            $body = "Имя: " . $name . "<br/>Email: " . $email . "<br/>Сообщение: " . $msg . "<br/>";
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['adminEmail'] => 'Swiss Commerce & Engineering'])
                ->setTo(Yii::$app->params['salesEmail'])
                ->setSubject('Сообщение с сайта Swiss Commerce & Engineering')
                ->setHtmlBody($body)
                ->send();
            echo json_encode(array(
                'success' => 1,
            ));
        }else{
            echo json_encode(array(
                'success' => 0,
            ));
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте вашу электронную почту для выполнения дальнейших инструкций.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'К сожалению, мы не в состоянии сбросить пароль для вашей электронной почты.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль был сохранен.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
