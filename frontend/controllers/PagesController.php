<?php

namespace frontend\controllers;

use backend\models\AboutGroup;
use Yii;
use backend\models\Pages;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends AppController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays a single Code model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($url)
    {
        $model = $this->findUrl($url);
        $title = (Yii::$app->language == 'ru') ? $model->title_ru : ((Yii::$app->language == 'en') ? $model->title_en : $model->title_kz);
        $keywords = (Yii::$app->language == 'ru') ? $model->keywords_ru : ((Yii::$app->language == 'en') ? $model->keywords_en : $model->keywords_kz);
        $description = (Yii::$app->language == 'ru') ? $model->desc_ru : ((Yii::$app->language == 'en') ? $model->desc_en : $model->desc_kz);
        $this->setMeta($title, $keywords, $description);
        $tab_model = AboutGroup::find()->all();
        return $this->render('view', [
            'model' => $model, 'tab_model' => $tab_model
        ]);
    }

    protected function findUrl($url)
    {
        if (($model = Pages::find()->where(['url' => $url, 'status' => 1])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
