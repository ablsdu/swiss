$(document).ready(function(){
    $('.grid').masonry({
      // options
      itemSelector: '.grid-item',
      columnWidth: '.grid-sizer',
      gutter: '.gutter-sizer'

    });

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 5,
        nav: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            460: {
                items: 2
            },
            767: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    $('.owl-prev').addClass('hidden');
    $('.owl-next').addClass('hidden');
    $('#slider_block2').on('mouseover', function(event){
        $('.owl-prev').removeClass('hidden');
        $('.owl-next').removeClass('hidden');
    });

    $('#slider_block2').on('mouseout', function(event){
        $('.owl-prev').addClass('hidden');
        $('.owl-next').addClass('hidden');
    });

    /*$('.contact_show a').click(function(e){
        e.preventDefault();
        $('#top .mail a').addClass('active');
        $('#top .tel a').addClass('active');
    });*/

     $('.scroll_btn').bind("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
    });

    $(window).scroll(function(){
        var button = $('.to_top_btn');
        var scrollPosition = $(window).scrollTop();
        if (scrollPosition > 800) {
            button.fadeIn("slow");
        } else {
            button.fadeOut("slow");
        }
    });

    $('.tab_nav .btn1').click(function(e){
        e.preventDefault();
        $(this).addClass('active');
        $('.tab_nav .btn2').removeClass('active');
        $('.tab_blocks .block1').show();
        $('.tab_blocks .block2').hide();
    });
    $('.tab_nav .btn2').click(function(e){
        e.preventDefault();
        $(this).addClass('active');
        $('.tab_nav .btn1').removeClass('active');
        $('.tab_blocks .block2').show();
        $('.tab_blocks .block1').hide();
    });

    $('.menu_open').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.menu').stop().slideToggle();
    });

    $('.cont_btn').click(function(e){
        e.preventDefault();
        $('#top .mail').toggleClass('active');
        $('#top .tel').toggleClass('active');
        $('#top').toggleClass('padtop');
        $(this).toggleClass('active');
        $('#header').toggleClass('big');
    });

    $(window).scroll(function(){
        if($(window).scrollTop() > 100) {
            $('#header').addClass('fix');
        }
        else {
            $('#header').removeClass('fix');
        }
    });


    $('.menu>ul>li>a').click(function(e){
        $(this).parents('li').find('.pod_menu').stop().slideToggle();
    });

    $(window).on('resize', function(){
        var win = $(this); //this = window

        if (win.width() < 800) {
            $(".menu-equipment").hide();
            $('#menu-equipment').removeClass('menu-equipment');
        }
        if (win.width() > 1000) {
            $(".menu-equipment").show();
            $(".menu-service").removeAttr("style");
            $('#menu-equipment').addClass('menu-equipment');
        }
    });

    if ($(this).width() < 800) {
        $('#menu-equipment').removeClass('menu-equipment');
    }
    if ($(this).width() > 1000) {
        $('#menu-equipment').addClass('menu-equipment');
    }

    $(".menu-equipment").show();
    $("img").mouseover(function(){
        $(".menu-equipment").hide();
    });
    $("img").mouseout(function(){
        $(".menu-equipment").show();
    });
    $(".table-responsive").mouseover(function(){
        $(".menu-equipment").hide();
    });
    $(".table-responsive").mouseout(function(){
        $(".menu-equipment").show();
    });
    $(".menu-service").mouseover(function(){
        $(".menu-equipment").hide();
    });
    $(".menu-service").mouseout(function(){
        $(".menu-equipment").show();
    });

    $(".all-box .box").equalHeights();
});