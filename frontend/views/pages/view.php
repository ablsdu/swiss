<?php
$this->registerJsFile('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');

$title = (Yii::$app->language == 'ru') ? $model->title_ru : ((Yii::$app->language == 'en') ? $model->title_en : $model->title_kz);
$content = (Yii::$app->language == 'ru') ? $model->content_ru : ((Yii::$app->language == 'en') ? $model->content_en : $model->content_kz);
?>
<section>
    <div id="page-header" style="<?= $model->header ? "background: url('$model->header')" : '';?>">
        <div class="block">
            <div class="container">
                <h1><?= $title;?></h1>
            </div>
        </div>
    </div>
</section>

<?php
if($model->id == 2) {
    ?>
    <section class="article-secure">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?= $content; ?>

                </div>
            </div>
        </div>
        <div class="tab-container hidden-xs">
            <ul class="nav nav-tabs">
                <?php
                $tab_count = 0;
                foreach ($tab_model as $tab) {
                    $image = (Yii::$app->language == 'ru') ? $tab->image_ru : ((Yii::$app->language == 'en') ? $tab->image_en : $tab->image_kz);
                    if ($tab->page_id == 2) {
                        $tab_count++;
                        if ($tab_count == 1) {
                            echo('
                                    <li class="active">
                                        <a data-toggle="tab" href="#' . $tab->id . '">
                                            <img src="' . $image . '" class="img-responsive">
                                        </a>
                                    </li>
                                    ');
                        } else {
                            echo('
                                    <li>
                                        <a data-toggle="tab" href="#' . $tab->id . '">
                                            <img src="' . $image . '" class="img-responsive">
                                        </a>
                                    </li>
                                    ');
                        }

                    }
                }
                ?>
            </ul>
        </div>
        <div class="container hidden-xs">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="tab-content" style="margin: auto; padding: 0 15px;">
                        <br/>
                        <br/>
                        <?php
                        $tab_count2 = 0;
                        foreach ($tab_model as $tab) {
                            if ($tab->page_id == 2) {
                                $tab_count2++;
                                $tab_content = (Yii::$app->language == 'ru') ? $tab->content_ru : ((Yii::$app->language == 'en') ? $tab->content_en : $tab->content_kz);
                                if ($tab_count2 == 1) {
                                    echo('
                        <div id="' . $tab->id . '" class="tab-pane fade in active">
                            ' . $tab_content . '
                        </div>
                        ');
                                } else {
                                    echo('
                        <div id="' . $tab->id . '" class="tab-pane fade">
                            ' . $tab_content . '
                        </div>
                        ');
                                }
                            }
                        }
                        ?>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
        <div class="container visible-xs">
            <div class="panel-group" id="accordion">
                <?php
                foreach ($tab_model as $tab) {
                    if ($tab->page_id == 2) {
                        $tab_content = (Yii::$app->language == 'ru') ? $tab->content_ru : ((Yii::$app->language == 'en') ? $tab->content_en : $tab->content_kz);
                        $image = (Yii::$app->language == 'ru') ? $tab->image_ru : ((Yii::$app->language == 'en') ? $tab->image_en : $tab->image_kz);
                        echo('
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="#collapse' . $tab->id . '">
                        <h4 class="panel-title collapsed" data-toggle="collapse" data-target="#collapse' . $tab->id . '">
                        <img src="' . $image . '" class="img-responsive" style="display: inline-block; display: block;
margin: 0 auto;"></h4>
                </div>
                <div class="panel-collapse collapse" id="collapse' . $tab->id . '">
                    <div class="panel-body">' . $tab_content . '</div>
                </div>
            </div>
            ');
                    }
                }
                ?>

            </div>
        </div>
    </section>

    <?php
    }else {

    ?>
    <section class="article-setran">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-article">
                        <?= $content; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}?>