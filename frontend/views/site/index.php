<?php

/* @var $this yii\web\View */
$this->title = 'Главная';
$this->registerMetaTag([
    'name' => 'description',
    'content' => '',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => '',
]);

$sliders = \backend\models\Slider::find()->all();
$slider1 = \backend\models\Slider::find()->one();
?>
<div id="main" class="container-fluid" style="background-image: url(<?= $slider1->image;?>);">
    <div class="container">
        <div class="row">
            <h1><?= Yii::t('main', 'Comprehensive Security Solutions'); ?></h1>
            <a href="/frontend/web/uploads/buklet/<?= Yii::$app->language;?>/prezentation.pdf" id="download-brochure" target="_blank"><?= Yii::t('main', 'Download brochure'); ?></a>
        </div>
    </div>
</div>
<script>
        var doSlider = function () {
            <?php
            $counter = 0;
            $counter_end = 0;

            foreach ($sliders as $key => $slider){
                $counter += 4000;
                echo ("
                    function second_passed".$key."() {
                        $('#main').css('background-image','url(".$slider->image.")');
                    }
                    setTimeout(second_passed".$key.", ".$counter.");
                ");
            }
            $counter_end = $counter + 4000;
            ?>

            setTimeout(doSlider, <?= $counter_end;?>);
        };
        doSlider();

</script>
<div id="grids" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="grid">
                <div class="grid-sizer"></div>
                <div class="gutter-sizer"></div>
                <?php
                foreach ($equipments as $eq){
                    $title = (Yii::$app->language == 'ru') ? $eq->title_ru : ((Yii::$app->language == 'en') ? $eq->title_en : $eq->title_kz);
                    echo ('
                    <div class="grid-item '.$eq->type.'">
                        <a href="/'.Yii::$app->language.'/equipment/'.$eq->url.'" id="equipment'.$eq->id.'">
                            <div style="background-image:url('.$eq->image.');">
                                <p>'.$title.'</p>
                            </div>
                        </a>
                    </div>
                    ');
                }
                ?>
            </div>
        </div>
    </div>
</div>
<div id="slider_block" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="slider_block" id="slider_block2">
                <h3>SWISS COMMERCE & ENGINEERING</h3>
                <h2><?= Yii::t('main', 'Comprehensive approach to complicated problem solving'); ?></h2>
                <div class="owl-carousel owl-theme">
                    <?php
                    $counter_service = 0;
                    foreach ($services as $s){
                        $counter_service++;
                        $title = (Yii::$app->language == 'ru') ? $s->title_ru : ((Yii::$app->language == 'en') ? $s->title_en : $s->title_kz);
                        $desc = (Yii::$app->language == 'ru') ? $s->desc_ru : ((Yii::$app->language == 'en') ? $s->desc_en : $s->desc_kz);
                        echo('
                        <div class="item">
                            <img src="/frontend/web/img/check.png" alt="">
                            <h4>'.$title.'</h4>
                            <p>'.$desc.'</p>
                            <a href="/'.Yii::$app->language.'/services/'.$s->url.'" id="service'.$counter_service.'">'.Yii::t('main', 'Read more').'</a>
                        </div>
                        ');
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="tab_main" class="container-fluid">
    <div class="container">
        <div class="row">
            <h4><?= Yii::t('main', 'Our company'); ?></h4>
            <h2><?= Yii::t('main', 'Experts in our field'); ?></h2>
            <ul class="tab_nav clear">
                <li><a href="#" class="btn1 active"><?= Yii::t('main', 'Our experience'); ?></a></li>
<!--                <li><a href="#" class="btn2">--><?//= Yii::t('main', 'Licences'); ?><!--</a></li>-->
            </ul>
            <div class="tab_blocks">
                <div class="block1 clear">
                    <div class="col-md-6">
                        <?php $about_text = (Yii::$app->language == 'ru') ? $about->hint_ru : ((Yii::$app->language == 'en') ? $about->hint_en : $about->hint_kz); ?>
                        <p><?= $about_text; ?></p>
                        <ul class="social clear">
                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                            <script src="//yastatic.net/share2/share.js"></script>
                            <div class="ya-share2" data-services="facebook,gplus,twitter"></div>
                        </ul>
                        <?php
                        $about_com = \backend\models\Pages::findOne(1);
                        ?>
                        <a href="/<?= Yii::$app->language; ?>/pages/<?= $about_com->url; ?>" id="more_about_company" class="more_about"><?= Yii::t('main', 'More about the company'); ?></a>
                    </div>
                    <div class="col-md-6">
                        <h3>
                            <?= Yii::t('main', 'SWISS COMMERCE & ENGINEERING offices'); ?><br>
                            <span>
                                <?= Yii::t('main', 'Astana'); ?>,
                                <?= Yii::t('main', 'Karaganda'); ?>,
                                <?= Yii::t('main', 'Ust-Kamenogorsk'); ?>,
                                <?= Yii::t('main', 'Almaty'); ?>,
                                <?= Yii::t('main', 'Atyrau'); ?>,
                                <?= Yii::t('main', 'Moscow'); ?></span>
                        </h3>
                        <img src="/frontend/web/img/cityes.png" alt="">
                        <span class="center_obs"><?= Yii::t('main', 'Service centers'); ?>:</span>
                        <ul class="cityes clear">
                            <li><?= Yii::t('main', 'Aktau'); ?></li>
                            <li><?= Yii::t('main', 'Aktobe'); ?></li>
                            <li><?= Yii::t('main', 'Almaty'); ?></li>
                            <li><?= Yii::t('main', 'Astana'); ?></li>
                            <li><?= Yii::t('main', 'Atyrau'); ?></li>
                            <li><?= Yii::t('main', 'Zhezkazgan'); ?></li>
                            <li><?= Yii::t('main', 'Karaganda'); ?></li>
                            <li><?= Yii::t('main', 'Kyzylorda'); ?></li>
                            <li><?= Yii::t('main', 'Kokshetau'); ?></li>
                            <li><?= Yii::t('main', 'Kostanay'); ?></li>
                            <li><?= Yii::t('main', 'Pavlodar'); ?></li>
                            <li><?= Yii::t('main', 'Petropavlovsk'); ?></li>
                            <li><?= Yii::t('main', 'Semey'); ?></li>
                            <li><?= Yii::t('main', 'Taldykorgan'); ?></li>
                            <li><?= Yii::t('main', 'Taraz'); ?></li>
                            <li><?= Yii::t('main', 'Uralsk'); ?></li>
                            <li><?= Yii::t('main', 'Ust-Kamenogorsk'); ?></li>
                            <li><?= Yii::t('main', 'Shymkent'); ?></li>
                        </ul>
                    </div>
                </div>
<!--                <div class="block2 clear">-->
                    <?php
//                    foreach ($certificates as $c){
//                        echo('
//                            <div class="col-md-4 col-sm-4">
//                                <a href="/backend/web/uploads/'.$c->link.'" target="_blank">
//                                    <img src="'.$c->image.'" alt="" class="in_img">
//                                    <img src="'.$c->image.'" alt="">
//                                </a>
//                            </div>
//                        ');
//                    }
                    ?>
<!--                </div>-->
            </div>
        </div>
    </div>
</div>
<div id="intessa" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="intessa clear">
                <div class="col-md-5 col-sm-6 col-xs-6">
                    <h4><?= Yii::t('main', 'Group of Companies'); ?></h4>
                    <h3>Intessa Group</h3>
                    <?php $group_text = (Yii::$app->language == 'ru') ? $group->hint_ru : ((Yii::$app->language == 'en') ? $group->hint_en : $group->hint_kz); ?>
                    <p><?= $group_text; ?></p>
                    <?php
                    $about_group = \backend\models\Pages::findOne(2);
                    ?>
                    <a href="/<?= Yii::$app->language; ?>/pages/<?= $about_group->url; ?>" id="learn-more"><?= Yii::t('main', 'Learn more'); ?></a>
                </div>
                <div class="intessa_logo col-md-7 col-sm-6 col-xs-6">
                    <img src="/frontend/web/img/intessa.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>