<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = Yii::t('main', 'Error 404!');
?>

<div class="container headerOffset">
    <div class="row">
        <div class="col-sm-12">
                <h1 class="text-center"><?= Yii::t('main', 'The page you requested was not found!');?></h1>
        </div><!-- col -->
    </div><!-- row -->
</div><!-- container -->

<div class="container">
    <div class="row">
        <div class="col-sm-12">

            <p class="text-center"><a class="btn btn-blue" href="/"><?= Yii::t('main', 'Go to Main page');?></a></p>
            <br/>
        </div><!-- col -->
    </div><!-- row -->
</div><!-- container -->