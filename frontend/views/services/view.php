<?php
$this->registerJsFile('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
$this->registerJsFile('/js/equipment.js');

$title = (Yii::$app->language == 'ru') ? $model->title_ru : ((Yii::$app->language == 'en') ? $model->title_en : $model->title_kz);
$content = (Yii::$app->language == 'ru') ? $model->content_ru : ((Yii::$app->language == 'en') ? $model->content_en : $model->content_kz);
?>
<section>
    <div id="page-header" style="<?= $model->header ? "background: url('$model->header')" : '';?>">
        <div class="block">
            <div class="container">
                <h1><?= $title;?></h1>
            </div>
        </div>
    </div>
</section>
<section class="article-setran">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-article">
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </div>
</section>