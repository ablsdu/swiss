<?php
$this->registerJsFile('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
$this->registerJsFile('/js/equipment.js');

$title = (Yii::$app->language == 'ru') ? $model->title_ru : ((Yii::$app->language == 'en') ? $model->title_en : $model->title_kz);
$content = (Yii::$app->language == 'ru') ? $model->content_ru : ((Yii::$app->language == 'en') ? $model->content_en : $model->content_kz);
?>
<section>
    <div id="page-header" style="<?= $model->header ? "background: url('$model->header')" : '';?>">
        <div class="block">
            <div class="container">
                <h1><?= $title;?></h1>
            </div>
        </div>
    </div>
</section>
<section class="article-setran">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-article">
                    <?= $content; ?>
                </div>
                <div class="companies-block">
                    <?php
                    foreach ($producers as $prod){
                        $title = (Yii::$app->language == 'ru') ? $prod->title_ru : ((Yii::$app->language == 'en') ? $prod->title_en : $prod->title_kz);
                        $hint = (Yii::$app->language == 'ru') ? $prod->hint_ru : ((Yii::$app->language == 'en') ? $prod->hint_en : $prod->hint_kz);

                        echo('
                            <div class="company-block row">
                                <div class="company-block-img">
                                    <img src="'.$prod->image.'" class="img-responsive centered" />
                                </div>
                                <div class="company-block-text">
                                    <h5>'.$title.'</h5>
                                    <span>'.$hint.'</span><br />
                                    <a href="/'. Yii::$app->language.'/producer/'.$prod->url.'">'.Yii::t('main', 'Read more').'</a>
                                </div>
                            </div>
                        ');
                    }

                    ?>
                    <div class="company-block row"></div>
                </div>
                <br/>
            </div>
        </div>
    </div>
</section>