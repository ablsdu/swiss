<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers;
use yii\helpers\Html;
use frontend\assets\ArticleAsset;
use frontend\widgets\WLang;
use backend\models\Code;

ArticleAsset::register($this);

$contacts = \backend\models\Contacts::findOne(1);
$services = \backend\models\Services::find()->all();
$equipments = \backend\models\Equipment::find()->all();

$code_head = Code::find()->where(['type' => 1])->all();
$code_foot = Code::find()->where(['type' => 2])->all();

$page1 = \backend\models\Pages::findOne(1);
$page2 = \backend\models\Pages::findOne(2);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::$app->getHomeUrl(); ?>favicon.ico" type="image/x-icon"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) . ' | ' . Yii::$app->name; ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::$app->getHomeUrl(); ?>favicon.ico" type="image/x-icon"/>
    <?php $this->head() ?>
    <?php
    foreach($code_head as $code){
        echo $code->content;
    }
    ?>
</head>
<body id="to-top">
<?php $this->beginBody() ?>

<a href="#to-top" class="to_top_btn scroll_btn">
    <?= Html::img('/frontend/web/img/to-top.png', ['alt' => 'SecureTech']) ?>
</a>
<div id="top" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="top">
                <div class="left clear">
                    <div class="mail">
                        <a href="mailto:<?= $contacts->email;?>"><?= $contacts->email;?></a>
                    </div>
                    <div class="tel">
                        <a href=""><?= $contacts->phone1;?>, <?= $contacts->phone2;?></a>
                    </div>
                    <div class="contact_show">
                        <a href="#" class="cont_btn" id="show-contacts">
                            <?php
                            if(Yii::$app->language == 'kz'){
                                ?>
                                <span class="first">
                                <span class="visible-sm visible-md visible-lg"><?= 'Байланыстар';?></span> <div class="visible-xs"><?= 'Байланыстар';?></div>
                            </span>
                                <?php
                            }else{
                                ?>
                                <span class="first">
                                <span><?= Yii::t('main', 'Show');?></span> <?= Yii::t('main', 'contacts');?>
                            </span>
                                <?php
                            }
                            ?>
                            <span class="second"><?= Yii::t('main', 'Hide');?></span>
                        </a>
                    </div>
                </div>
                <div class="right">
                    <a href="#requestModal" data-toggle="modal" id="get-price-quote"><span><?= Yii::t('main', 'Get');?></span> <?= Yii::t('main', 'a price quote');?></a>
                    <ul class="clear hidden-xs">
                        <?= WLang::widget(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="header" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="header clear">
                <div class="logo">
                    <a href="/">
                        <?= Html::img('/frontend/web/img/logo.png', ['alt' => 'Swiss Commerce & Engineering']) ?>
                    </a>
                </div>
                <a href="#" class="menu_open">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <div class="menu">
                    <ul class="clear">
                        <li>
                            <a href="/<?= Yii::$app->language;?>/pages/<?= $page1->url;?>"><?= Yii::t('main', 'About company'); ?></a>
                        </li>
                        <li>
                            <a href="#" class="menu-service"><?= Yii::t('main', 'Equipment'); ?></a>
                            <ul class="pod_menu menu-service">
                                <?php
                                foreach($equipments as $equipment){
                                    $title = (Yii::$app->language == 'ru') ? $equipment->title_ru : ((Yii::$app->language == 'en') ? $equipment->title_en : $equipment->title_kz);
                                    echo('
                                    <li><a href="/'. Yii::$app->language.'/equipment/'.$equipment->url.'" class="menu-service">'.$title.'</a></li>
                                ');
                                }
                                ?>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><?= Yii::t('main', 'Services'); ?></a>
                            <ul class="pod_menu menu-equipment" id="menu-equipment">
                                <?php
                                foreach($services as $service){
                                    $title = (Yii::$app->language == 'ru') ? $service->title_ru : ((Yii::$app->language == 'en') ? $service->title_en : $service->title_kz);
                                    echo('
                                    <li><a href="/'. Yii::$app->language.'/services/'.$service->url.'">'.$title.'</a></li>
                                ');
                                }
                                ?>
                            </ul>
                        </li>
                        <li>
                            <a href="/<?= Yii::$app->language;?>/pages/<?= $page2->url;?>"><?= Yii::t('main', 'Group of Companies'); ?></a>
                        </li>
                    </ul>
                    <ul class="clear visible-xs hidden-md hidden-sm menu_solcial">
                        <?= WLang::widget(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CONTENT -->
<?= $content ?>
<!-- CONTENT -->
<div id="footer" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="footer clear">
                <div class="center col-md-4">
                    <h4><?= Yii::t('main', 'Solutions'); ?></h4>
                    <ul>
                        <?php
                        foreach($equipments as $equipment){
                            $title = (Yii::$app->language == 'ru') ? $equipment->title_ru : ((Yii::$app->language == 'en') ? $equipment->title_en : $equipment->title_kz);
                            echo('
                                    <li><a href="/'. Yii::$app->language.'/equipment/'.$equipment->url.'">'.$title.'</a></li>
                                ');
                        }
                        ?>
                    </ul>
                </div>
                <div class="left col-md-4">
                    <h4><?= Yii::t('main', 'Contacts'); ?></h4>
                    <div class="address"><?= (Yii::$app->language == 'ru') ? $contacts->address_ru : ((Yii::$app->language == 'en') ? $contacts->address_en : $contacts->address_kz);?></div>
                    <div class="tel"><?= $contacts->phone1;?><?= $contacts->phone2;?></div>
                    <div class="email"><?= $contacts->email;?></div>
                    <div class="copypast">SWISS COMMERCE & ENGINEERING, <?= date('Y');?></div>
                </div>
                <div class="right col-md-4">
                    <h4><?= Yii::t('main', 'Message'); ?></h4>
                    <form id="msgForm" novalidate="novalidate" class="fv-form fv-form-bootstrap">
                        <div class="control-group form-group">
                            <div class="controls">
                                <input class="form-control" name="name1" id="name1" placeholder="<?= Yii::t('main', 'Your name'); ?>" data-fv-field="name" type="text">
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <input class="form-control" name="email1" id="email1" placeholder="<?= Yii::t('main', 'Your e-mail'); ?>" type="text">
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <textarea class="form-control" name="msg1" id="msg1" placeholder="<?= Yii::t('main', 'Message'); ?>"  cols="100" rows="3"></textarea>
                            </div>
                        </div>
                        <input type="submit" id="send-footer" value="<?= Yii::t('main', 'Send'); ?>">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <br>
                <h4 class="modal-title"><?= Yii::t('main', 'Get a price quote'); ?></h4>
                <br>
                <p class="text-center"><?= Yii::t('main', 'Please, select the contact category below that best matches your needs. Our manager will prepare an individual commercial offer for you.'); ?></p>
            </div>
            <div class="modal-body">
                <form id="requestForm" novalidate="novalidate" class="fv-form fv-form-bootstrap">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="control-group form-group col-md-12">
                                <div class="controls">
                                    <input class="form-control" name="name" id="name" placeholder="<?= Yii::t('main', 'Your name'); ?>" data-fv-field="name" type="text">
                                </div>
                            </div>
                            <div class="control-group form-group col-md-6">
                                <div class="controls">
                                    <input class="form-control" name="email" id="email" placeholder="<?= Yii::t('main', 'Your e-mail'); ?>" type="text">
                                </div>
                            </div>
                            <div class="control-group form-group col-md-6">
                                <div class="controls">
                                    <input class="form-control phone" name="phone" id="phone" placeholder="<?= Yii::t('main', 'Your phone'); ?>" data-fv-field="phone" type="text">
                                </div>
                            </div>
                            <div class="control-group form-group col-md-12">
                                <div class="controls">
                                    <textarea class="form-control" name="msg" id="msg" placeholder="<?= Yii::t('main', 'Message'); ?>"  cols="100" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <button type="submit" id="send-modal" class="btn btn-modal"><?= Yii::t('main', 'Send'); ?></button>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="success-requestModal" tabindex="-1" role="dialog" aria-labelledby="Welcome back" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <p><?= Yii::t('main', 'Thank you!'); ?> <?= Yii::t('main', 'Your request is accepted.'); ?>  <?= Yii::t('main', 'Our manager will contact you in the near future.'); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" id="close-modal" class="btn" data-dismiss="modal"><?= Yii::t('main', 'Close'); ?></button>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
<script type="application/javascript">
    $(document).ready(function() {
        $('#requestForm').formValidation({
            framework: 'bootstrap',
            err: {
                container: '#messages'
            },
//            icon: {
//                valid: 'glyphicon glyphicon-ok',
//                invalid: 'glyphicon glyphicon-remove',
//                validating: 'glyphicon glyphicon-refresh'
//            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Введите Имя'
                        }
                    }
                },
                phone: {
                    validators: {
                        stringLength: {
                            min:8,
                            message: 'Поле Телефон должно содержать более 8 символов'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Введите Email'
                        },
                        emailAddress: {
                            message: 'Введите верный Email'
                        }
                    }
                },
            }
        });

        $('#msgForm').formValidation({
            framework: 'bootstrap',
            err: {
                container: '#messages'
            },
//            icon: {
//                valid: 'glyphicon glyphicon-ok',
//                invalid: 'glyphicon glyphicon-remove',
//                validating: 'glyphicon glyphicon-refresh'
//            },
            fields: {
                name1: {
                    validators: {
                        notEmpty: {
                            message: 'Введите Имя'
                        }
                    }
                },
                email1: {
                    validators: {
                        notEmpty: {
                            message: 'Введите Email'
                        },
                        emailAddress: {
                            message: 'Введите верный Email'
                        }
                    }
                },
            }
        });
    });
    $(document).ready(function() {
        $('#requestForm').on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $.ajax({
                type: 'get',
                url: '/site/call',
                data: $('form').serialize(),
                success: function () {
                    // alert('form was submitted');
                }
            });

            var validator = $(e.target).data('formValidation');
            $('#requestModal')
                .one('hidden.bs.modal', function() {
                    $('#success-requestModal')
                        .find('.name')
                        .html(validator.getFieldElements('name').val()).end()
                        .modal('show');
                })
                .modal('hide');
        });

        $('#requestModal').on('shown.bs.modal', function() {
            $('#requestForm').formValidation('resetForm', true)
                .find('[name="name"]').focus();
        });

        $('#msgForm').on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
            $('html,body').animate({scrollTop: 0}, 'slow');

            $.ajax({
                type: 'get',
                url: '/site/message',
                data: $('#msgForm').serialize(),
                success: function (data) {
                    var parsed = JSON.parse(data);
                    if(parsed.success){
                        $('#success-requestModal').modal('show');
                        document.getElementById("msgForm").reset();
                    }else{
                        alert('Ошибка при отправке сообщения');
                    }
                }
            });
        });

    });
</script>
<div style="display: none;">
    <?php
    foreach($code_foot as $code){
        echo $code->content;
    }
    ?>
</div>
</body>
</html>
<?php $this->endPage() ?>
